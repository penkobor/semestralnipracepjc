# Řešení soustav lin. rovnic

Aplikace na vstupu dostane rozšířenou matici soustavy a na výstup vypíše vektor jejího řešení, případně zprávu o tom, že řešení neexistuje. Pokud je řešení nekonečně mnoho, program vypíše jedno partikulární řešení a bázi lineárního prostoru řešení přidružené homogenní soustavy.

# Kompilace

`cmake <path/to/project>` <br />
`make`

# Help

`./le-solver --help`

# Spuštení:

`./le-solver --filein /Path/To/File --stdout --threads 2` <br />

Čtení ze souboru, vypis odpovědí, ve dvou vlaknech <br />

# Formát vstupního souboru:

m n  <br />
a_1_1 ... a_1_m-1 ... b<br />
.   .        .<br />
.       .    .<br />
.            .<br />
a_n_1 ... a_n_m-1 ... b<br />

kde **m** je počet **řádků**, **n** je počet **sloupců** v rozšířené matici<br />

# Přiklad vstupního souboru

3 3<br />
1 2 3<br />
4 5 6<br />
7 8 9<br />

první řadek (3 3) -> počet řadku a sloupcu v matici <br />
sloupcí [1,4,7],[2,5,8] - koeff. na x_1 a x_2<br />
sloupec [3,6,9] - vektor b<br />

<br />

**test1.txt a test2.txt** - soubory ve správném vstupním formátu pro kontrolu funkčnosti programu<br />

# Vstup z input streamů

Když je zadán příznak `--stdin`, znamená to, že data jsou převzata ze standardního vstupního proudu<br />

**Format vstupu ve streamu je stejný jako v souboru:**<br />
`<počet řadku m> <počet sloupců n> <a_1_1> <a_1_2> ... <a_1_n> ... <a_m_1> ... <a_m_n>`<br />

Čísla lze oddělit přechodem na novy řadek, tabulátory a mezery. <br />

Pro spuštení zadejte přikaz<br /> `./le-solver --stdin --stdout threads 2`<br />

Program se spustí a bude čekat na data ze standardního vstupního proudu.<br />
<br />
# Měření

**Měření času běhu programu se dá provést pomocí příkazu `time`.**<br />
Například:<br />
`time ./le-solver --filein /Path/To/File --stdout --threads 2`<br />

Měření bylo vykonáno na processoru Intel Core i5 (5250U) a program s jedním vláknem provedl výpočet za 0.010s a z dvěma za 0.009s


