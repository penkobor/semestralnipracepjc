#ifndef GJE_SOLVER_HPP_
#define GJE_SOLVER_HPP_

#include <map>
#include <vector>

class GaussJordanElimination {
 public:
    GaussJordanElimination()
        : number_of_threads_(1) {}
    GaussJordanElimination(size_t number_of_threads)
        : number_of_threads_(number_of_threads) {}
    ~GaussJordanElimination() {}

    size_t solve(double **matrix, size_t row, size_t col, double **answ, double **base, size_t *base_size);

 private:
    size_t number_of_threads_;

    size_t SearchElement(double **matrix,
                          std::map<size_t, size_t> &elements,
                          size_t col_index,
                          size_t row);

    static void *SearchElement_(void *args);
    struct search_element_data {
        pthread_t handler;
        double **matrix;
        std::map<size_t, size_t> *elements;
        size_t col_index;
        size_t row_start;
        size_t row_end;
        size_t answer;
    };

    void TransformMatrix(double **matrix, size_t row_index, size_t col_index, size_t row, size_t col);

    static void *TransformMatrix_(void *args);
    struct transform_matrix_data {
        pthread_t handler;
        double **matrix;
        size_t row_index;
        size_t col_index;
        size_t row_start;
        size_t row_end;
        size_t col_start;
        size_t col_end;
    };

    size_t ExtractAnswer(double **matrix,
                         size_t row,
                         size_t col,
                         std::map<size_t, size_t> elements,
                         std::vector<size_t> bad_col,
                         double **answ,
                         double **base,
                         size_t *base_size);

    bool SolutionExists(double **matrix,
                        size_t row,
                        size_t col,
                        std::map<size_t,size_t> elements);

};

#endif // GJE_SOLVER_HPP_
