#include <iostream>
#include <fstream>

#include <string.h>

#include "gje_solver.hpp"

static void no_opt_error(std::string opt){
    std::cout << "No options for " << opt << std::endl;
    std::exit(EXIT_FAILURE);
}

static char* getopt(int argc, char* argv[], const char* opt, bool with_arg) {
    for (size_t i = 1; i < argc; i++){
        if (!strcmp(opt, argv[i])){
            if (with_arg) {
                if (i + 1 < argc) {
                    return argv[i + 1];
                } else {
                    no_opt_error(opt);
                }
            } else {
                return *argv;
            }
        }
    }
    return 0;
}

static void show_usage(std::string name){
    std::cout << "Usage:" << name << " <option(s)>\n"
              << "Options:\n"
              << "\t--help:\n"
              << "\t\t" << "Show this help message\n"
              << "\t--filein <path/to/file>:\n"
              << "\t\tinput from file" << "\n"
              << "\t--fileout <path/to/file>:\n"
              << "\t\toutput to file" << "\n"
              << "\t--stdin:\n"
              << "\t\tstandart input" << "\n"
              << "\t--stdout:\n"
              << "\t\tstandart output" << "\n"
              << "\t--threads <number>:\n"
              << "\t\tnumber of threads" << "\n";
    std::exit(EXIT_FAILURE);
}

static int le_solver_main(std::istream *in, std::ostream *out, size_t threads){
    
    
    //Vytvoření objektu pro řešení SLR
    GaussJordanElimination solver(threads);

    size_t row;
    size_t col;

    //Čteme ze vstupu počet řádků a sloupců
    *in >> row >> col;

    //Uvolnime paměť pro matici koeficientů
    double **matrix = new double*[row];
    for (size_t i = 0; i < row; i++)
        matrix[i] = new double[col];

    //Vyplnime matici koeficientů
    for (size_t i = 0; i < row; i++){
        for(size_t j = 0; j < col; j++){
            *in >> matrix[i][j];
        }
    }

    //Uvolnime pamet' pro odpoved'
    double **answ = new double*[2];
    for (size_t i = 0; i < 2; i++)
        answ[i] = new double[col - 1];

    //Uvolnime pamet' pro vectory prostoru reseni
    double **base = new double*[col - 1];
    for (size_t i = 0; i < col - 1; i++)
        base[i] = new double[col - 1];

    size_t base_size;
    //Spustime reseni
    size_t answ_type = solver.solve(matrix, row, col, answ, base, &base_size);
    //Vypisujeme reseni v zavistlosti na odpovedi
    //0 - nema reseni;
    //1 - 1 reseni;
    //2 - vice reseni
    switch(answ_type){
    	case 0:
            *out << "no solution" << std::endl;
            break;
    	case 1:
            *out << "answer:" << std::endl;
            for (size_t i = 0; i < col - 1; i++) {
                *out << answ[0][i] << " ";
            }
            *out << std::endl;
            break;
    	case 2:
            *out << "private: ";
            for (size_t i = 0; i < col - 1; i++) {
                *out << answ[0][i] << " ";
            }
            *out << std::endl;
            for (size_t i = 0; i < base_size; i++) {
                *out << "base[" << i << "]: ";
                for(size_t j = 0; j < col - 1; j++){
                    *out << base[i][j] << " ";
                }
                *out << std::endl;
            }
            break;
    }

    //smaze answ, base  a matrix
    for (size_t i = 0; i < col - 1; i++){
        delete base[i];
    }
    delete[] base;

    for (size_t i = 0; i < 2; i++)
        delete answ[i];
    delete[] answ;

    for (size_t i = 0; i < row; i++)
        delete matrix[i];
    delete[] matrix;
    
    return 0;
}

int main(int argc, char* argv[]){
    //--help
    if(getopt(argc, argv,"--help", false)) {
        show_usage(argv[0]);
    }


    //set input stream
    std::istream* in = NULL;
    char* in_filename = getopt(argc, argv,"--filein", true);
    if(in_filename){
        std::ifstream* fin = new std::ifstream(in_filename);

        if(fin->is_open()){
            in = fin;
        } else {
            std::cout << "err\n";
        }

    } else if (getopt(argc,argv,"--stdin", false)) {
        in = &std::cin;
    }

    if(!in){
        std::cout << "no input\n";
        show_usage(argv[0]);
        std::exit(-1);
    }

    //set output stream
    std::ostream* out = NULL;
    char* out_filename = getopt(argc, argv,"--fileout", true);
    if(out_filename){
        std::ofstream* fout = new std::ofstream(out_filename);

        if(fout->is_open()){
            out = fout;
        } else {
            std::cout << "err\n";
        }

    } else if (getopt(argc,argv,"--stdout", false)) {
        out = &std::cout;
    }

    if(!out) {
        std::cout << "no out\n";
        show_usage(argv[0]);
        std::exit(-1);
    }

    //set number of threads
    size_t threads = 0;
    char* threads_raw = getopt(argc, argv,"--threads", true);
    if(threads_raw) {
        threads = atoi(threads_raw);
    }

    if(threads < 1)
        threads = 1;

    return le_solver_main(in, out, threads);
}
