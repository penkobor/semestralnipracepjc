#include <iostream>
#include <algorithm>
#include <map>
#include <vector>
#include <cmath>
#include <pthread.h>

#include "gje_solver.hpp"

size_t GaussJordanElimination::solve(double **matrix, size_t row, size_t col, double **answ, double **base, size_t *base_size){
    std::map<size_t, size_t> elements; //map s poctem radku a sloupcu rozhodujicich elementu
    std::vector<size_t> bad_col; //ukladame cisla sloupcu bez rozhodujicich elementu
    for(size_t i = 0; i < col - 1; i++) { 
        size_t row_index = this->SearchElement(matrix, elements, i, row); //Hledame index radku s rozhodujicim prvkem v i-m sloupcu
        //Ked' ten prvek jsme nenasli, pridavame index sloupcu do bad_col 
        if (row_index == -1){
            bad_col.push_back(i);
            continue;
        }
        elements[row_index] = i; //Pridavame indexy nalezeneho prvku v elements 

        this->TransformMatrix(matrix, row_index, i, row, col); //Upravujeme matici
    }

    return this->ExtractAnswer(matrix, row, col, elements, bad_col, answ, base, base_size); //vratime typ odpovedi
}

size_t GaussJordanElimination::SearchElement(double **matrix, std::map<size_t, size_t> &elements, size_t col_index, size_t row) {
    size_t number_of_threads = this->number_of_threads_;
    if(number_of_threads > row - 1)
        number_of_threads = row - 1;

    search_element_data data[number_of_threads]; 

    //Vytvořte pole s hranicemi pro vlakna
    size_t scopes[number_of_threads + 1];
    size_t step = (row - 1) / number_of_threads;
    scopes[0] = 0;
    scopes[1] = ((row - 1) % step) + step + 1;
    for (size_t i = 2; i <= number_of_threads; i++){
        scopes[i] = scopes[i - 1] + step;
    }

    //Plnění kontextu pole
    for(size_t i = 0; i < number_of_threads; i++){
        data[i].matrix = matrix;
        data[i].elements = &elements;
        data[i].col_index = col_index;
        data[i].row_start = scopes[i];
        data[i].row_end = scopes[i + 1];
    }

    //Vytvoreni vlaken
    for (size_t i = 0; i < number_of_threads; i++){
        pthread_create(&data[i].handler, NULL, GaussJordanElimination::SearchElement_, (void *) &data[i]);
    }

    //Hledani nejlepsiho rozhodujiciho prvku mezi nalezenymi ve vlaknach 
    size_t element_row_index = -1;
    double element_rate = std::numeric_limits<double>::max();
    for(size_t i = 0; i < number_of_threads; i++){
        pthread_join(data[i].handler, NULL);

        if(data[i].answer == -1)
            continue;
        double elem = matrix[data[i].answer][col_index];
        double current_element_rate = abs(abs(elem) - 1);
        if (current_element_rate < element_rate) {
            element_row_index = data[i].answer;
            element_rate = current_element_rate;
        }
    }

    return element_row_index;
}

void *GaussJordanElimination::SearchElement_(void *ptr) {
    search_element_data *data = (search_element_data*)ptr;

    //Hledani nejlepsiho rozhodujiciho prvku v diapozonu sloupcu
    size_t element_row_index = -1;
    double element_rate = std::numeric_limits<double>::max();
    for(size_t i = data->row_start; i < data->row_end; i++) {
        if(data->matrix[i][data->col_index] == 0)
            continue;

        if(data->elements->count(i) != 0) 
            continue;

        double elem = data->matrix[i][data->col_index];
        double current_element_rate = abs(abs(elem) - 1);
        if (current_element_rate < element_rate) {
            element_row_index = i;
            element_rate = current_element_rate;
        }
    }

    data->answer = element_row_index;
    pthread_exit(NULL);
}

void GaussJordanElimination::TransformMatrix(double **matrix, size_t row_index, size_t col_index, size_t row, size_t col) {
    size_t row_start = 0;
    size_t row_end = row;
    size_t col_start = 0;
    size_t col_end = col;

    double divisor = matrix[row_index][col_index];
    for (size_t i = col_index; i < col_end; i++){
        matrix[row_index][i] /= divisor;
    }

    size_t number_of_threads = this->number_of_threads_;
    if(number_of_threads > row - 1)
        number_of_threads = row - 1;

    transform_matrix_data data[number_of_threads];

    size_t row_scopes[number_of_threads + 1];
    size_t step = (row - 1) / number_of_threads;
    row_scopes[0] = 0;
    row_scopes[1] = ((row - 1) % step) + step + 1;
    for (size_t i = 2; i <= number_of_threads; i++){
        row_scopes[i] = row_scopes[i - 1] + step;
    }

    for(size_t i = 0; i < number_of_threads; i++){
        data[i].matrix = matrix;
        data[i].row_index = row_index;
        data[i].col_index = col_index;
        data[i].row_start = row_scopes[i];
        data[i].row_end = row_scopes[i + 1];
        data[i].col_start = 0;
        data[i].col_end = col;
    }

    //Vytvoreni vlaken
    for (size_t i = 0; i < number_of_threads; i++){
        pthread_create(&data[i].handler, NULL, GaussJordanElimination::TransformMatrix_, (void *) &data[i]);
    }

    for(size_t i = 0; i < number_of_threads; i++){
        pthread_join(data[i].handler, NULL);
    }
}

void *GaussJordanElimination::TransformMatrix_(void *ptr){
    transform_matrix_data *data = (transform_matrix_data *) ptr;

    double multiplier;
    for (size_t i = data->row_start; i < data->row_end; i++) {
        if (i == data->row_index)
            continue;
        if (data->matrix[i][data->col_index] == 0)
            continue;
        multiplier = data->matrix[i][data->col_index];
        for(size_t j = data->col_start; j < data->col_end; j++) {
            data->matrix[i][j] -= data->matrix[data->row_index][j] * multiplier;
        }
    }

    pthread_exit(NULL);
}

bool GaussJordanElimination::SolutionExists(double **matrix, size_t row, size_t col, std::map<size_t, size_t> elements) {
    for(size_t i = 0; i < row; i++) {
        if (matrix[i][col - 1] != 0)
            if (elements.find(i) == elements.end())
                return false;
    }
    return true;
}


size_t GaussJordanElimination::ExtractAnswer(double **matrix, size_t row, size_t col, std::map<size_t, size_t> elements, std::vector<size_t> bad_col, double **answ, double **base, size_t *base_size) {
    if(!this->SolutionExists(matrix, row, col, elements))
        return 0;

    if(bad_col.size() == 0) {
        for (std::map<size_t, size_t>::iterator it = elements.begin(); it != elements.end(); it++){
            answ[0][it->second] = matrix[it->first][col - 1];
        }
        return 1;
    }

    for (size_t i = 0; i < 2; i++){
        for (size_t j; j < col - 1; j++){
            answ[i][j] = std::nan("");
        }
    }


    *base_size = bad_col.size();
    for (size_t i = 0; i < bad_col.size(); i++){
        for(size_t j = 0; j < col - 1; j++){
            base[i][j] = 0;
        }
    }

    for (std::map<size_t, size_t>::iterator it = elements.begin(); it != elements.end(); it++){
        answ[0][it->first] = matrix[it->second][col - 1];
    }

    for(size_t i = 0; i < bad_col.size(); i++){
        answ[0][bad_col[i]] = 0;
    }

    for(size_t i = 0; i < bad_col.size(); i++){
        size_t index = bad_col[i];
        for (std::map<size_t, size_t>::iterator it = elements.begin(); it != elements.end(); it++){
            base[i][it->first] = matrix[it->second][index] * -1;
        }
        base[i][index] = 1;
    }
    return 2;
}